let destination = document.getElementById('d1');
let text = '';

function createNewDiv(content, styleClass) {
    let newDiv = document.createElement('div');
    newDiv.className = styleClass;
    let textNode = document.createTextNode(content);
    newDiv.appendChild(textNode);
    destination.appendChild(newDiv);
}
//Kata 1- Display the numbers from 1 to 20. (1, 2, 3, …,19, 20)
createNewDiv('Kata 1- Display the numbers from 1 to 20. (1, 2, 3, …,19, 20)', 'heading')
text = '';
for (let i = 1; i <= 20; i++) {
    text = text + i + " ";
}
createNewDiv(text, 'content');

//Kata 2- Display the even numbers from 1 to 20. (2, 4, 6, …, 18, 20)

createNewDiv('Kata 2- Display the even numbers from 1 to 20. (2, 4, 6, …, 18, 20)', 'heading')
text = '';
for (i = 2; i <= 20; i++) {
    if (i % 2 == 0);
    text = text + i + " ";
}
createNewDiv(text, 'content')


//Kata 3- Display the odd numbers from 1 to 20. (1, 3, 5, …, 17, 19)

createNewDiv('Kata 3- Display the odd numbers from 1 to 20. (1, 3, 5, …, 17, 19)', 'heading')
text = '';
for (i = 1; i <= 20; i++) {
    if (i % 2 !== 0);
    text = text + i + " ";
}
createNewDiv(text, 'content')


//Kata 4- Display the multiples of 5 up to 100. (5, 10, 15, …, 95, 100)

createNewDiv('Kata 4- Display the multiples of 5 up to 100. (5, 10, 15, …, 95, 100)', 'heading')
text = '';
for (i = 5; i <= 100; i++) {
    if (i % 5 == 0);
    text = text + i + " ";
}
createNewDiv(text, 'content')

//Kata 5- Display the square numbers from 1 up to 100. (1, 4, 9, …, 81, 100)

createNewDiv('Kata 5- Display the square numbers from 1 up to 100. (1, 4, 9, …, 81, 100)', 'heading')
text = '';
for (i = 1; i <= 10; i++) {
    text = text + i + " ";
}
createNewDiv(text, 'content')


//Kata 6- Display the numbers counting backwards from 20 to 1. (20, 19, 18, …, 2, 1)

createNewDiv('Kata 6- Display the numbers counting backwards from 20 to 1. (20, 19, 18, …, 2, 1)', 'heading')
text = '';
for (i = 20; i >= 1; i--) {
    text = text + i + " ";
}
createNewDiv(text, 'content')

//Kata 7- Display the even numbers counting backwards from 20 to 1. (20, 18, 16, …, 4, 2)

createNewDiv('Kata 7- Display the even numbers counting backwards from 20 to 1. (20, 18, 16, …, 4, 2)', 'heading')
text = '';
for (i = 20; i >= 2; i--) {
    if (i % 2 == 0)
        text = text + i + " ";
}
createNewDiv(text, 'content')

//Kata 8- Display the odd numbers from 20 to 1, counting backwards. (19, 17, 15, …, 3, 1)

createNewDiv('Kata 8- Display the odd numbers from 20 to 1, counting backwards. (19, 17, 15, …, 3, 1)', 'heading')
text = '';
for (i = 19; i >= 1; i--) {
    if (i % 2 !== 0)
        text = text + i + " ";
}
createNewDiv(text, 'content')

//Kata 9- Display the multiples of 5, counting down from 100 to 1. (100, 95, 90, …, 10, 5)

createNewDiv('Kata 9- Display the multiples of 5, counting down from 100 to 1. (100, 95, 90, …, 10, 5)', 'heading')
text = '';
for (i = 100; i >= 5; i--) {
    if (i % 5 == 0)
        text = text + i + " ";
}
createNewDiv(text, 'content')

//Kata 10- Display the square numbers, counting down from 100. (100, 81, 64, …, 4, 1)

createNewDiv('Kata 10- Display the square numbers, counting down from 100. (100, 81, 64, …, 4, 1)', 'heading')
text = '';
for (i = 10; i >= 1; i--) {
    text = text + i + " ";
}
createNewDiv(text, "content")

//const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];

//Kata 11- Display the 20 elements of sampleArray. (469, 755, 244, …, 940, 472)

createNewDiv('Kata 11- Display the 20 elements of sampleArray. (469, 755, 244, …, 940, 472)', 'heading')
text = '';
var twentyEl = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472]
text = text + twentyEl + " ";
createNewDiv(text, "content")

//Kata 12- Display all the even numbers contained in sampleArray. (244, 758, 450, …, 940, 472)

createNewDiv('Kata 12- Display all the even numbers contained in sampleArray. (244, 758, 450, …, 940, 472)', 'heading')
text = '';
var evenArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472]

for (let i = 0; i < evenArray.length; i++) {
    const arr_value = evenArray[i]
    if (arr_value % 2 == 0) {
        text = text + arr_value + " ";
    }
}
createNewDiv(text, "content")


//Kata 13- Display all the odd numbers contained in sampleArray. (469, 755, 245, …, 179, 535)

createNewDiv('Kata 13- Display all the odd numbers contained in sampleArray. (469, 755, 245, …, 179, 535)', 'heading')
text = '';
var oddArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472]
for (let i = 1; i < oddArray.length; i++) {
    const arr_value = oddArray[i]
    if (arr_value % 2 !== 0) {
        text = text + arr_value + " ";
    }
}
createNewDiv(text, "content")

//Kata 14- Display the square of each element in sampleArray. (219961, 570025, …, 222784)

createNewDiv('Kata 14- Display the square of each element in sampleArray. (219961, 570025, …, 222784)', 'heading')
text = '';
var sqArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472]
for (let i = 1; i < sqArray.length; i++) {
    const arr_value = sqArray[i]
    const sq_arr_value = arr_value * arr_value
    text = text + sq_arr_value + " ";

}
createNewDiv(text, "content")


//Kata 15- Display the sum of all the numbers from 1 to 20.

createNewDiv('Kata 15- Display the sum of all the numbers from 1 to 20', 'heading')
text = '';
var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
var sum = 0;
for (var i = 0; i < numbers.length; i++) {
    sum = sum + numbers[i];
}
text = sum

createNewDiv(text, "content")

//Kata 16- Display the sum of all the elements in sampleArray.

createNewDiv('Kata 16- Display the sum of all the elements in sampleArray', 'heading')
text = '';
var sumArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472]
var sum = 0;
for (var i = 0; i < sumArray.length; i++) {
    sum = sum + sumArray[i];
}
createNewDiv(sum, "content")


//Kata 17- Display the smallest element in sampleArray.
createNewDiv('Kata 17- Display the smallest element in sampleArray.', 'heading')
var sumSmall = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472]
var minValue = sumSmall[0]
for (var i = 0; i < sumArray.length; i++) {
    if (sumSmall[i] < minValue) {
        minValue = sumSmall[i]
    }
}
createNewDiv(minValue, "content")



//Kata 18- Display the largest element in sampleArray.

createNewDiv('Kata 18- Display the largest element in sampleArray.', 'heading')
text = '';
var largeArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472]
let maxValue = 0
for (var i = 0; i < largeArray.length; i++) {
    if (largeArray[i] > maxValue) {
        maxValue = largeArray[i]
    }
}
createNewDiv(maxValue, "content")


//Kata 19- Display 20 solid gray rectangles, each 20px high and 100px wide.

createNewDiv('Kata 19- Display 20 solid gray rectangles, each 20px high and 100px wide.', 'heading')

for (var i = 0; i < 20; i++) {
    let bar = document.createElement('div')
    bar.style.height = "20px"
    bar.style.width = "100px"
    bar.style.backgroundColor = "gray"
    bar.style.margin = "15px"
    destination.appendChild(bar)
}


//Kata 20- Display 20 solid gray rectangles, each 20px high, with widths ranging evenly from 105px to 200px (remember #4, above).


createNewDiv('Kata 20- Display 20 solid gray rectangles, each 20px high, with widths ranging evenly from 105px to 200px (remember #4, above).', 'heading')

for (var i = 0; i < 20; i++) {
    let bar = document.createElement('div')
    bar.style.height = "20px"
    bar.style.width = 105 + i*5 + 'px'
    bar.style.backgroundColor = "gray"
    bar.style.margin = "15px"
    destination.appendChild(bar)
}


//Kata 21- Display 20 solid gray rectangles, each 20px high, with widths in pixels given by the 20 elements of sampleArray.

createNewDiv('Kata 21- Display 20 solid gray rectangles, each 20px high, with widths in pixels given by the 20 elements of sampleArray)', 'heading')


for (var i = 0; i < 20; i++) {
    let bar = document.createElement('div')
    bar.style.height = "20px"
    bar.style.width = largeArray[i] + 'px'
    bar.style.backgroundColor = "gray"
    bar.style.margin = "15px"
    destination.appendChild(bar)
}

//Kata 22- As in #21, but alternate colors so that every other rectangle is red.

createNewDiv('Kata 22- As in #21, but alternate colors so that every other rectangle is red.', 'heading')

for (var i = 0; i < 20; i++) {
    let bar = document.createElement('div')
    bar.style.height = "20px"
    bar.style.width = largeArray[i] + 'px'
    if (i % 2 == 0){
        bar.style.backgroundColor = 'gray'
    } else {
        bar.style.backgroundColor = 'red'
    }
    bar.style.margin = "15px"
    destination.appendChild(bar)
}


//Kata 23- As in #21, but color the rectangles with even widths red.

createNewDiv('Kata 23- As in #21, but color the rectangles with even widths red.', 'heading')

for (var i = 0; i < 20; i++) {
    let bar = document.createElement('div')
    bar.style.height = "20px"
    bar.style.width = largeArray[i] + 'px'
    if (largeArray[i] % 2 == 0){
        bar.style.backgroundColor = 'red'
    } else {
        bar.style.backgroundColor = 'gray'
    }
    bar.style.margin = "15px"
    destination.appendChild(bar)
}
